package me.flyray.bsin.utils;

import java.util.Map;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.exception.BusinessException;

public class BsinPageUtil {

    public static void pageNotNull(Map<String, Object> pagination){
        EmptyChecker.isEmpty(pagination);
        Integer pageNum = (Integer)pagination.get("pageNum");
        Integer pageSize = (Integer)pagination.get("pageSize");
        if (pagination.isEmpty() || pageNum == null || pageSize == null){
            throw new BusinessException(ResponseCode.PAGE_NUM_ISNULL);
        }
    }
}
