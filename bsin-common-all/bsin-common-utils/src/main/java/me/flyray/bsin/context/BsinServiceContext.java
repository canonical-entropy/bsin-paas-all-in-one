package me.flyray.bsin.context;

import com.alibaba.fastjson.JSON;

import java.util.Map;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class BsinServiceContext {
    public static <T> T getReqBodyDto(Class<T> dtoClassType,Map<String, Object> map) {
            BaseService.validate(dtoClassType,map);
            String jsonStr = JSON.toJSONString(map);
        return JSON.parseObject(jsonStr, dtoClassType);
    }

    public static <T> T bisId(Class<T> dtoClassType,Map<String, Object> map) {
            BaseService.bsinId(dtoClassType,map);
            String jsonStr = JSON.toJSONString(map);
        T obj = JSON.parseObject(jsonStr, dtoClassType);
        return obj;
    }
}
