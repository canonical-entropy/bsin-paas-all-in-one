# bsin-paas服务端部署文档

[toc]

## **环境准备**

*  特别说明

**请务必按照本文档部署运行章节进行操作，避免踩坑弯路！！**



*  环境说明

|   工具    | 版本  |     备注      |
| :-------: | :---: | :-----------: |
|    JDK    |  11   |  必须11及以上：推进11或是14             |
|   MySQL   |  8.0  | 建议使用MySQL |
|   IDEA    |   | 建议使用IDEA  |
| zookeeper | 3.6.2 |               |
|   seata   | 1.4.2 |               |



##  **服务端代码部署** 

*  特别说明

**无论您是多年编程的高级工程师，还是刚刚入门的实习生，部署请完全参考本文档操作，避免踩坑弯**



### 一、项目下载

> 项目下载

```
git clone https://gitee.com/s11e-DAO/bsin-paas-all-in-one.git
```


### 二、初始化数据库

> 参数说明

```
版本： mysql8.0
默认字符集: utf8mb4
默认排序规则: utf8mb4_general_ci
```

> 脚本说明

```
bsin-server-upms\doc\bsin-upms.sql   upms权限管理脚本
bsin-server-targe-gateway\doc\bsin-gateway.sql  网关脚本
```


### 三、配置修改

1、数据库源信息修改（修改upms和网关resources下的application.properties文件）

```
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/bsin-upms?characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&allowMultiQueries=true&zeroDateTimeBehavior=convertToNull #修改成自己创建的数据库
spring.datasource.username=***  #用户名
spring.datasource.password=***  #密码

```

2、zookeeper配置修改(确认upms和网关application.properties中zookeeper的IP地址是否为本机)

```
# zookeeper配置
com.alipay.sofa.rpc.registry.address=zookeeper://127.0.0.1:2181   
```

3、修改seata地址(bsin-server-targe-gateway)resources下的file.conf文件

```
service {
  #transaction service group mapping
  vgroupMapping.my_test_tx_group = "default"
  #only support when registry.type=file, please don't set multiple addresses
  #本地环境需要修改
  default.grouplist = "127.0.0.1:8091"			#确定IP是否为本机
  #degrade, current not support
  enableDegrade = false
  #disable seata
  disableGlobalTransaction = false
}
```

### 四、安装依赖
安装 网关工程下的依赖包：bsin-server-targe-gateway\doc\lib

### 五、中间件启动
前提：配置好zookeeper和seata

1、先启动zookeeper

```java
在zookeeper的bin目录下输入命令
 
./zkServer.sh start   启动命令
```
也可以双击zkServer.cmd启动

2、启动seata

```
在seata的bin目录下输入命令

./seata-server.bat -p 8091 -h 127.0.0.1 -m file   启动命令
```
也可以双击seata-server.bat启动
3、redis 暂时不需要

### 六、导入工程
导入以下工程：
* bsin-common-all (需编译通过安装到本地maven库中)
* bsin-paas-dependencies (需编译通过安装到本地maven库中)
* bsin-server-targe-gateway
* bsin-server-upms

### 七、项目启动

```Java
//先启动网关
bsin-server-targegateway\src\main\java\me\flyray\bsin\gateway\BsinGatewayApplication.java
//继续启动upms
upms-server\src\main\java\me\flyray\bsin\server\BsinUpmsApplication.java
```

### 八、网关说明
默认端口8097

1、请求报文格式：
```
{
  "serviceName":"服务名称",
  "serviceMethod": "方法名称",
  "sign":"2",
  "requestSerialNo":"请求流水号",
  "timestamp":"请求时间戳",
  "bizParams": {
    
  }
}
```
2、响应报文
```
{
  "data": {
    "token": ""
  },
  "code": "000000",
  "message": "SUCCESS"
}
或
{
  "data": [
    {
        "token": "c"
    },
    {
        "token": ""
    },
  ],
  "code": "000000",
  "message": "SUCCESS"
}
```

3、Authorization：是通过getAccessToken接口获取到的accessToken
将 Authorization 放到请求头作为请求参数
